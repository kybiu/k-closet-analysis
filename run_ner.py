from denver.learners import FlairSequenceTaggerLearner
model_path = "models/ApolloEntityExtractor_latest.pt"
learn = FlairSequenceTaggerLearner(mode='inference', model_path=model_path)

# Get prediction
data_df = learn.predict_on_df(data='./data/query_knowledge_base_all_cluster_only_text.csv',
                                    text_cols='text',
                                    is_normalize=True)

data_df.to_csv('outfile.csv', index=False, encoding='utf-8')
