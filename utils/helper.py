import re
import emoji
import pandas as pd
import os
import glob


def remove_col_str(messsage):
    new_customer_message = messsage.replace('\n', '')

    new_customer_message = new_customer_message.replace(' &#.*', '')
    return new_customer_message


def remove_col_white_space(messsage):
    # remove white space at the beginning of string
    new_customer_message = messsage.strip()
    return new_customer_message


def de_emojify(message):
    regrex_pattern = re.compile(pattern="["
                                        u"\U0001F600-\U0001F64F"  # emoticons
                                        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                                        u"\U0001F680-\U0001F6FF"  # transport & map symbols
                                        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                                        "]+", flags=re.UNICODE)
    try:
        deemojy_message = regrex_pattern.sub(r'', message)
        return deemojy_message
    except:
        return message


def export_chatlog_on_specific_date():
    k_closet_df = pd.read_csv("../data/Kcloset.csv")
    date_list = k_closet_df["fix_time"].to_list()
    k_closet_df["date"] = [x[:10] if isinstance(x, str) else "" for x in date_list]
    k_closet_df["time"] = [x[11:] if isinstance(x, str) else "" for x in date_list]

    aug_list = [1, 3, 11, 19, 21, 27]
    sep_list = [1, 7, 13, 16, 18, 24, 27]
    oct_list = [1, 2, 7, 12, 20]

    df_list = []
    for date in oct_list:
        if len(str(date)) == 1:
            date = "0" + str(date)
        full_date = "2020-10-" + str(date)
        sub_df = k_closet_df[k_closet_df["date"] == full_date]
        df_list.append(sub_df)

    a = pd.concat(df_list)
    a.to_csv("oct.csv", index=False)


def merge_csv_in_folder():
    extension = 'csv'
    all_filenames = [i for i in glob.glob('../data/output_intent/*.{}'.format(extension))]
    # combine all files in the list
    combined_csv = pd.concat([pd.read_csv(f) for f in all_filenames])
    # export to csv
    combined_csv.to_csv("combined_intent.csv", index=False, encoding='utf-8-sig')


def reformat_chatlog_for_visualize(df: pd.DataFrame):
    df = df[["conversation_id", "customer", "predicted", "fix_time", "label"]]
    df = df.dropna(subset=["customer"]).reset_index(drop=True)
    df = df.rename(columns={"customer": "sentence"})
    df = df[df["predicted"] != ""]
    df.to_csv("k_closet_conversation_24h_reformat.csv", index=False)
    return df


def mapping_predict_to_chatlog(chatlog_df: pd.DataFrame, predict_df: pd.DataFrame):
    chatlog_df.insert(3, 'predicted', "")
    for row in predict_df.itertuples():
        message_index = row.message_index
        pred_intent = row.pred
        chatlog_df.at[message_index, "predicted"] = pred_intent
    sub_df = chatlog_df[(chatlog_df["predicted"] == "") & ~(chatlog_df["customer"].isna())]
    for row in sub_df.itertuples():
        message_index = row.message_index
        text = row.customer
        if text in ["Bắt đầu", "Get Started", "Chat với Admin", "Có ai đang online để chat không?"]:
            chatlog_df.at[message_index, "predicted"] = "greet"
        if "https" in text:
            chatlog_df.at[message_index, "predicted"] = "image"
    reformat_chatlog_for_visualize(chatlog_df)
    return chatlog_df
