import pandas as pd
from tqdm import tqdm

conversation_24h = pd.read_csv('./data/k_closet_conversation_24h.csv')
conversation_48h = pd.read_csv('./data/k_closet_conversation_48h.csv')

def turn_avg(df):
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    conversation_ids = df["conversation_id"]
    no_turn_of_conversation = []
    for id in tqdm(conversation_ids):
        sub_df  = df[df["conversation_id"] == id]
        no_turn = sub_df["turn"].max() + 1
        no_turn_of_conversation.append(no_turn)

    turn_average = sum(no_turn_of_conversation) / len(no_turn_of_conversation)
    print(turn_average)
    return turn_average

def count_img_conversation(df):
    conversation_ids = df["conversation_id"]
    img_conversation = 0
    for id in tqdm(conversation_ids):
        sub_df = df[df["conversation_id"] == id]
        # customer_message = sub_df["customer"].dropna().to_list
        customer_message =list(sub_df["customer"].dropna())
        if customer_message == 0:
            continue
        customer_message = " ".join(customer_message)
        if "scontent" in customer_message:
            img_conversation += 1
            continue
    print(img_conversation)
    return img_conversation




