import pandas as pd
from denver.learners import ULMFITClassificationLearner
from denver.learners import OnenetLearner
import time
import re
from pprint import pprint
from utils.helper import *
from collections import Counter
import csv
from datetime import datetime


def find_urls(string):
    regex_urls = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    url = re.findall(regex_urls, string)
    return [x[0] for x in url]


def pre_process_data(input_df:pd.DataFrame):
    """
    Loại nhưng mesage mà khahcs có message là bắt đầu, get started, và gửi ảnh  ....
    :param input_df:
    """
    # k_closet_df = pd.read_csv("data/k_closet_conversation_24h.csv")
    k_closet_df = input_df

    # date_list = k_closet_df["fix_time"].to_list()
    # k_closet_df["date"] = [x[:10] if isinstance(x, str) else "" for x in date_list]
    # k_closet_df["time"] = [x[11:] if isinstance(x, str) else "" for x in date_list]
    k_closet_df = k_closet_df.dropna(subset=["customer"])

    # Loại các message đặc biệt và gửi link ảnh
    k_closet_df_without_image = k_closet_df[~k_closet_df["customer"].str.contains("https")]
    k_closet_df_without_image = k_closet_df_without_image[~k_closet_df_without_image["customer"].str.contains("Bắt đầu")]
    k_closet_df_without_image = k_closet_df_without_image[~k_closet_df_without_image["customer"].str.contains("Get Started")]
    k_closet_df_without_image = k_closet_df_without_image[~k_closet_df_without_image["customer"].str.contains("Chat với Admin")]
    k_closet_df_without_image = k_closet_df_without_image[~k_closet_df_without_image["customer"].str.contains("Có ai đang online để chat không?")]

    # loại emoji
    k_closet_df_without_image["customer"] = k_closet_df_without_image["customer"].apply(remove_col_str)
    k_closet_df_without_image["customer"] = k_closet_df_without_image["customer"].apply(remove_col_white_space)
    k_closet_df_without_image["customer"] = k_closet_df_without_image["customer"].apply(de_emojify)
    k_closet_df_without_image["customer"] = k_closet_df_without_image["customer"].str.lower()

    # lọai các message chỉ có sđt
    k_closet_df_without_image = k_closet_df_without_image[k_closet_df_without_image["customer"] != ""]
    k_closet_df_without_image = k_closet_df_without_image[~k_closet_df_without_image['customer'].str.isdigit()]
    k_closet_df_without_image = k_closet_df_without_image[~k_closet_df_without_image['customer'].str.isnumeric()]

    k_closet_df_without_image.to_csv("k_closet_without_img", index=False)


def ulm_intent_predict():
    model_path = './models/denver-vicls.pkl'
    learn = ULMFITClassificationLearner(mode="inference", model_path=model_path)
    #    k_conversation_24h = pd.read_csv("data/k_closet_conversation_24h.csv")
    #    k_conversation_24h.rename(columns={'Unnamed: 0': 'message_index'}, inplace=True)
    data_df = learn.predict_on_df(data="./data/k_closet_conversation_24h.csv",
                                  text_cols='customer',
                                  lowercase=True,
                                  rm_special_token=True,
                                  rm_url=True,
                                  rm_emoji=True)

    data_df.to_csv("ulm_intent_prediction_result.csv", index=False, encoding="utf-8-sig")


def sub_ic():
    model_path = './models/vi_sub_class.pkl'
    learn = ULMFITClassificationLearner(mode="inference", model_path=model_path)
    data_df = learn.predict_on_df(data='./utils/combined_csv.csv',
                                  text_cols='text',
                                  lowercase=True,
                                  rm_special_token=True,
                                  rm_url=True,
                                  rm_emoji=True)

    data_df.to_csv("sub_ic_result.csv", index=False, encoding="utf-8-sig")


def onenet_intent_predict():
    model_path = "./models/denver-onenet_latest.tar.gz"
    learn = OnenetLearner(mode='inference', model_path=model_path)
    data_df = learn.predict_on_df(data='./data/kcloset_without_img.csv',
                                  text_cols='customer',
                                  lowercase=True,
                                  rm_special_token=True,
                                  rm_url=True,
                                  rm_emoji=True)
    data_df.to_csv("onenet_intent_prediction_result.csv", index=False, encoding="utf-8-sig")


def analyze_onenet_result():
    onenet_df = pd.read_csv("result/onenet_intent_prediction_result.csv")
    onenet_df = onenet_df[onenet_df["score"] >= 0.9]
    onenet_df = onenet_df[onenet_df['text'].str.split().str.len() > 1]
    onenet_df = onenet_df[onenet_df["text"] != "bên bạn có còn sản phẩm này không"]

    intent_dict = dict(Counter(onenet_df["intent_pred"].to_list()))

    with open('onenet_intent_count_df.csv', 'w') as csv_file:
        writer = csv.writer(csv_file)
        for key, value in intent_dict.items():
            writer.writerow([key, value])


def find_link():
    k_closet_df = pd.read_csv("data/Kcloset.csv")
    all_link = []
    all_link_message = k_closet_df[k_closet_df["customer"].str.contains("https")]
    normal_link = k_closet_df[(k_closet_df["customer"].str.contains("https")) & ~(k_closet_df["customer"].str.contains("https://scontent"))].reset_index()
    scontent_link = k_closet_df[k_closet_df["customer"].str.contains("https://scontent")].reset_index()


def label_conversation(fb_conversations):
    fb_conversations.insert(0, 'conversation_id', "")
    fmt = '%Y-%m-%d %H:%M:%S'

    ids = []

    sender_ids = list(fb_conversations["sender_id"])
    to_sender_ids = list(fb_conversations["to_sender_id"])

    for i, sender_id in enumerate(sender_ids):
        if sender_id == 1454523434857990:
            ids.append(to_sender_ids[i])
        else:
            ids.append(sender_id)

    ids = sorted(set(ids), key=ids.index)
    conversation_id = 0
    checked_sender_id = []

    for sender_id_index, sender_id in enumerate(ids):
        if sender_id not in checked_sender_id:
            conversation_id += 1
            checked_sender_id.append(sender_id)

        sub_df = fb_conversations[(fb_conversations["sender_id"] == sender_id) | (fb_conversations["to_sender_id"] == sender_id)].reset_index()

        for index, item in sub_df.iterrows():
            message_index = item["index"]
            fb_conversations.at[message_index, "conversation_id"] = conversation_id
            if index + 1 < len(sub_df):
                next_message = sub_df.iloc[index + 1]
                current_time = item["created_time"][:10] + " " + item["created_time"][11:19]
                current_time = datetime.strptime(current_time, fmt)

                next_time = next_message["created_time"][:10] + " " + next_message["created_time"][11:19]
                next_time = datetime.strptime(next_time, fmt)

                time_diff = (next_time - current_time).total_seconds()
                if time_diff > 86400:
                    conversation_id += 1

    return fb_conversations


def export_chatlog_2020():
    k_closet_df = pd.read_csv("data/Kcloset.csv")
    date_list = k_closet_df["fix_time"].to_list()
    k_closet_df["date"] = [x[:10] if isinstance(x, str) else "" for x in date_list]
    k_closet_df["time"] = [x[11:] if isinstance(x, str) else "" for x in date_list]
    k_closet_df_2020 = k_closet_df[k_closet_df["date"].str.contains("2020")]
    return k_closet_df_2020


def split_conversation(type: str):
    """
    đọc file csv raw chatlog rồi chia thành conversation và turn
    :param type:
    :return:
    """
    time_diff_threshold = 86400
    if type == "48h":
        time_diff_threshold = 172800

    k_closet_df = pd.read_csv("data/kcloset_2020.csv")
    # Remove row bot customer and shop is nan
    k_closet_df = k_closet_df[~((k_closet_df["customer"].isna()) & (k_closet_df["shop"].isna()))].reset_index(drop=True)

    # Remove row shop reply comment
    k_closet_df = k_closet_df[~((k_closet_df["shop"].notna()) & (k_closet_df["shop"].str.contains("Bạn đang trả lời bình luận")))].reset_index(drop=True)
    k_closet_df.insert(0, "conversation_id", "")
    datetime_fmt = '%Y-%m-%d %H:%M:%S'
    conversation_id = 0
    checked_sender_id = []

    all_sender_id = k_closet_df[k_closet_df["label"] != 'K\'s Closet']["label"].to_list()
    for row in k_closet_df.itertuples():
        row_index = row.Index
        sender_id = row.label
        current_time = row.fix_time
        current_time = datetime.strptime(current_time, datetime_fmt)

        if sender_id != 'K\'s Closet' and sender_id not in checked_sender_id:
            conversation_id += 1
            checked_sender_id.append(sender_id)
        try:
            k_closet_df.at[row_index, "conversation_id"] = conversation_id
        except:
            a = 0
        if row_index + 1 < len(k_closet_df):
            next_row = k_closet_df.loc[row_index + 1]
            next_row_sender_id = next_row["label"]
            if next_row_sender_id != 'K\'s Closet' and next_row_sender_id not in checked_sender_id:
                continue
            next_time = next_row["fix_time"]
            next_time = datetime.strptime(next_time, datetime_fmt)

            time_diff = (next_time - current_time).total_seconds()

            if time_diff > time_diff_threshold:
                conversation_id += 1
    k_closet_df = split_conversation_to_turn(k_closet_df)
    k_closet_df.to_csv("k_closet_conversation_" + type + ".csv", index=False)
    return k_closet_df

def split_conversation_to_turn(df):
    df.insert(1, "turn", "")
    conversation_ids = list(df["conversation_id"])
    conversation_ids = list(dict.fromkeys(conversation_ids))
    for id in conversation_ids:
        sub_df = df[df["conversation_id"] == id]
        turn = 0
        previous_index = 0
        first_item_in_sub_df = True
        for index, item in sub_df.iterrows():
            if not first_item_in_sub_df:
                previous_sender_name = sub_df.at[previous_index, "label"]
                current_sender_name = item["label"]
                try:
                    if previous_sender_name == 'K\'s Closet' and current_sender_name != previous_sender_name:
                        turn += 1
                except:
                    a = 0
            first_item_in_sub_df = False
            previous_index = index
            df.at[index, "turn"] = turn
    return df


def main():
    start = time.time()
    k_closet_df = split_conversation("24h")
    pre_process_data(k_closet_df)


    # sub_ic()
    # predict_intent = pd.read_csv("utils/combined_intent.csv")
    # mapping_predict_to_chatlog(conversation_24_df, predict_intent)
    print("Run time: " + str(time.time() - start))


if __name__ == '__main__':
    main()
